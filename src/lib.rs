#![deny(missing_docs)]

/*!
This crate simplifies writing data to and reading data from streams.
You just need to implement the traits `ToStream` and `FromStream`.
You might have to implement value specific traits to custom settings types.
**/

mod arrays;
mod bool;
/// Settings for collection streaming.
pub mod collections;
/// Settings for number streaming.
pub mod numbers;
mod wrappers;

use std::io::{Read, Result, Write};

/// A trait for types, which can be written to streams.
/// Most argument types might need some settings.
pub trait ToStream<S> {
    /// Writes a value of this type to a stream. Return an error on failure.
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()>;
}

/// A trait for types, which can be read from streams.
pub trait FromStream<S>: Sized {
    /// Reads a value of this type from a stream and returns it. Returns an error on failure instead.
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self>;
}

#[inline(always)]
/// A simple function to write a value to a stream.
pub fn to_stream<S, V: ToStream<S> + ?Sized, W: Write>(value: &V, stream: &mut W) -> Result<()> {
    value.to_stream(stream)
}

#[inline(always)]
/// A simple function to read a value from a stream.
pub fn from_stream<S, V: FromStream<S>, R: Read>(stream: &mut R) -> Result<V> {
    V::from_stream(stream)
}

/// Some simple default settings for streaming.
pub mod default_settings;
