use crate::{from_stream, numbers::EndianSettings, to_stream, FromStream, ToStream};

use std::io::{Read, Result, Write};

impl<S, T: ToStream<S>, const N: usize> ToStream<S> for [T; N] {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()> {
        for element in self {
            element.to_stream(stream)?
        }

        Ok(())
    }
}

impl<S, T: FromStream<S> + Default + Copy, const N: usize> FromStream<S> for [T; N] {
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self> {
        let mut result = [Default::default(); N];

        for element in &mut result {
            *element = from_stream(stream)?
        }

        Ok(result)
    }
}

impl<S: EndianSettings, T: ToStream<S>> ToStream<S> for [T] {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()> {
        to_stream::<S, _, _>(&self.len(), stream)?;
        for element in self {
            element.to_stream(stream)?
        }

        Ok(())
    }
}
