use crate::{from_stream, to_stream, FromStream, ToStream};

use std::io::{Read, Result as IOResult, Write};

impl<S, T: ToStream<S>> ToStream<S> for Box<T> {
    fn to_stream<W: Write>(&self, stream: &mut W) -> IOResult<()> {
        (**self).to_stream(stream)
    }
}

impl<S, T: FromStream<S>> FromStream<S> for Box<T> {
    fn from_stream<R: Read>(stream: &mut R) -> IOResult<Self> {
        Ok(Self::new(from_stream(stream)?))
    }
}

impl<S, T: ToStream<S>> ToStream<S> for Option<T> {
    fn to_stream<W: Write>(&self, stream: &mut W) -> IOResult<()> {
        match self {
            None => to_stream::<S, _, _>(&false, stream),
            Some(value) => {
                to_stream::<S, _, _>(&true, stream)?;
                value.to_stream(stream)
            }
        }
    }
}

impl<S, T: FromStream<S>> FromStream<S> for Option<T> {
    fn from_stream<R: Read>(stream: &mut R) -> IOResult<Self> {
        Ok(if from_stream::<S, bool, _>(stream)? {
            None
        } else {
            Some(from_stream(stream)?)
        })
    }
}

impl<S, T: ToStream<S>, E: ToStream<S>> ToStream<S> for Result<T, E> {
    fn to_stream<W: Write>(&self, stream: &mut W) -> IOResult<()> {
        match self {
            Err(err) => {
                to_stream::<S, _, _>(&false, stream)?;
                err.to_stream(stream)
            }
            Ok(value) => {
                to_stream::<S, _, _>(&true, stream)?;
                value.to_stream(stream)
            }
        }
    }
}

impl<S, T: FromStream<S>, E: FromStream<S>> FromStream<S> for Result<T, E> {
    fn from_stream<R: Read>(stream: &mut R) -> IOResult<Self> {
        Ok(if from_stream::<S, bool, _>(stream)? {
            Err(from_stream(stream)?)
        } else {
            Ok(from_stream(stream)?)
        })
    }
}
