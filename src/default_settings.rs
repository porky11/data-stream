use crate::{
    collections::{Size, SizeSettings},
    numbers::{Endian, EndianSettings},
};

/// Default settings for efficiency on a single platform.
pub struct NativeSettings;
/// Default settings for portability when possibly used on different platforms.
pub struct PortableSettings;

impl EndianSettings for NativeSettings {
    const ENDIAN: Endian = Endian::Native;
}

impl EndianSettings for PortableSettings {
    const ENDIAN: Endian = Endian::Little;
}

impl SizeSettings for NativeSettings {
    const SIZE: Size = Size::Usize;
}

impl SizeSettings for PortableSettings {
    const SIZE: Size = Size::U32;
}
