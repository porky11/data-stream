mod shared;

use data_stream::numbers::{BigEndian, LittleEndian, NativeEndian};
use shared::assert_streamed_eq;

macro_rules! impl_test {
    ($name: ident, $t: ty, $endian: ty) => {
        #[test]
        fn $name() {
            assert_streamed_eq::<$endian, _>(&(123 as $t));
        }
    };
}

impl_test!(check_u8_be, u8, BigEndian);
impl_test!(check_u16_be, u16, BigEndian);
impl_test!(check_u32_be, u32, BigEndian);
impl_test!(check_u64_be, u64, BigEndian);
impl_test!(check_u128_be, u128, BigEndian);

impl_test!(check_u8_le, u8, LittleEndian);
impl_test!(check_u16_le, u16, LittleEndian);
impl_test!(check_u32_le, u32, LittleEndian);
impl_test!(check_u64_le, u64, LittleEndian);
impl_test!(check_u128_le, u128, LittleEndian);

impl_test!(check_u8_ne, u8, NativeEndian);
impl_test!(check_u16_ne, u16, NativeEndian);
impl_test!(check_u32_ne, u32, NativeEndian);
impl_test!(check_u64_ne, u64, NativeEndian);
impl_test!(check_u128_ne, u128, NativeEndian);

impl_test!(check_i8_be, i8, BigEndian);
impl_test!(check_i16_be, i16, BigEndian);
impl_test!(check_i32_be, i32, BigEndian);
impl_test!(check_i64_be, i64, BigEndian);
impl_test!(check_i128_be, i128, BigEndian);

impl_test!(check_i8_le, i8, LittleEndian);
impl_test!(check_i16_le, i16, LittleEndian);
impl_test!(check_i32_le, i32, LittleEndian);
impl_test!(check_i64_le, i64, LittleEndian);
impl_test!(check_i128_le, i128, LittleEndian);

impl_test!(check_i8_ne, i8, NativeEndian);
impl_test!(check_i16_ne, i16, NativeEndian);
impl_test!(check_i32_ne, i32, NativeEndian);
impl_test!(check_i64_ne, i64, NativeEndian);
impl_test!(check_i128_ne, i128, NativeEndian);

impl_test!(check_f32_be, f32, BigEndian);
impl_test!(check_f64_be, f64, BigEndian);

impl_test!(check_f32_le, f32, LittleEndian);
impl_test!(check_f64_le, f64, LittleEndian);

impl_test!(check_f32_ne, f32, NativeEndian);
impl_test!(check_f64_ne, f64, NativeEndian);

impl_test!(check_usize_be, usize, BigEndian);
impl_test!(check_usize_le, usize, LittleEndian);
impl_test!(check_usize_ne, usize, NativeEndian);
