This library is meant to be the easiest and most straightforward way for writing data directy to streams (`impl Write`) and reading data directly from streams (`impl Read`).

It contains the traits `ToStream` and `FromStream`, which need to be implemented for a type to be written to or read from a stream.

# Why?

There is no good binary serialization library.
The most common way seems to be `bincode`, which is based on `serde`.
`serde` is bloat, especially when just writing binary data. It has a data model and stores the name of everything.

As long as you can derive your traits, you won't notice most of the complexity, but when you have to implement some trait manually in `serde`, it can get overcomplicated.

In this library you only have to write or read the elements to the stream in order, when you want to implement some trait manually.
And since streams are used by default, it should be pretty straightforward for interactions with the file system or networking.

# Features and progress

Which data types are already implemented or planned to be implemented?

- [x] Booleans (`bool`)
- [x] Numbers (`u8` to `u128`, `i8` to `i128`, `f32` and `f64`, `usize`)
- [x] Fixed size arrays (`[T; N]`, generic over type and size)
- [ ] Characters (`char`)
- [ ] Strings (`String`)
- [x] Simple wrapper types (`Box`, `Option`, `Result`)
- [x] Collections (`Vec`, `VecDeque`, `LinkedList`, `HashMap`, `BTreeMap`, `HashSet`, `BTreeSet`, `BinaryHeap`)

Which types probably won't be implemented?

- non-static types (also `&[T]`, `&str`)
- tuples (since there's no generic way to implement them)

Other planned features:

- derive traits

